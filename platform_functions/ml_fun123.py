from xpms_helper.model.data_schema import DatasetFormat, DatasetConvertor
import pandas as pd
from xpms_helper.model import model_utils

file_name = "model.pkl"


def run(datasets,config):
    dataset = DatasetConvertor.convert(datasets, DatasetFormat.DATA_FRAME, None)
    run_df = dataset["value"]
    target_column = "class"
    X = run_df.loc[:, run_df.columns != target_column]
    file_name = "model1.pkl"
    model_obj = model_utils.load(file_name=file_name,config=config)
    predictions = model_obj.predict_proba(X)
    result_df = pd.DataFrame(data=predictions,columns=model_obj.classes_)
    result_dataset = {"value": result_df, "data_format": "data_frame"}
    return result_dataset
run(datasets= "iris__classification_half_half_Evaluate.csv",config= "uma.csv")


